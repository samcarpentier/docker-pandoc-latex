FROM ubuntu:16.04

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-xetex \
    texlive-fonts-recommended \
    latex-xcolor \
    lmodern \
    fontconfig \
    make \
    mercurial \
    git \
    ca-certificates \
    wget \
    libgmp10

RUN wget https://github.com/jgm/pandoc/releases/download/2.2.3.2/pandoc-2.2.3.2-1-amd64.deb && dpkg -i pandoc-2.2.3.2-1-amd64.deb
