# docker-pandoc-latex

A simple ubuntu-based [docker image](https://hub.docker.com/r/samcarpentier/pandoc-latex/) with Pandoc and LaTeX that can be used for PDF generation.

Pandoc version: **2.2.3.2**
